package edu.towson.cis.cosc442.project1.monopoly.gui;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import edu.towson.cis.cosc442.project1.monopoly.Player;

public class PlayerPanelData {
	public JButton btnBuyHouse;
	public JButton btnDrawCard;
	public JButton btnEndTurn;
	public JButton btnGetOutOfJail;
	public JButton btnPurchaseProperty;
	public JButton btnRollDice;
	public JButton btnTrade;
	public JLabel lblMoney;
	public JLabel lblName;
	public Player player;
	public JTextArea txtProperty;

	public PlayerPanelData() {
	}
}