package edu.towson.cis.cosc442.project1.monopoly.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import edu.towson.cis.cosc442.project1.monopoly.TradeDeal;

public class GUITradeDialogData {
	public JButton btnOK;
	public JButton btnCancel;
	public JComboBox<Object> cboSellers;
	public JComboBox<Object> cboProperties;
	public TradeDeal deal;
	public JTextField txtAmount;

	public GUITradeDialogData() {
	}
}