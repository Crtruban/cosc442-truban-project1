package edu.towson.cis.cosc442.project1.monopoly.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import edu.towson.cis.cosc442.project1.monopoly.*;

public class MainWindow extends JFrame implements MonopolyGUI{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MainWindowData data = new MainWindowData(new JPanel(), new ArrayList<GUICell>(), new JPanel(), new JPanel(), new JPanel());

	public MainWindow() {
		data.northPanel.setBorder(new LineBorder(Color.BLACK));
		data.southPanel.setBorder(new LineBorder(Color.BLACK));
		data.westPanel.setBorder(new LineBorder(Color.BLACK));
		data.eastPanel.setBorder(new LineBorder(Color.BLACK));
		
		Container c = getContentPane();
		//setSize(800, 600);
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension d = tk.getScreenSize();
		setSize(d);
		c.add(data.northPanel, BorderLayout.NORTH);
		c.add(data.southPanel, BorderLayout.SOUTH);
		c.add(data.eastPanel, BorderLayout.EAST);
		c.add(data.westPanel, BorderLayout.WEST);
		
		this.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
	
	private void addCells(JPanel panel, List<?> cells) {
		for(int x=0; x<cells.size(); x++) {
			GUICell cell = new GUICell((Cell)cells.get(x));
			panel.add(cell);
			data.guiCells.add(cell);
		}
	}
	
	private void buildPlayerPanels() {
		GameMaster master = GameMaster.instance();
		JPanel infoPanel = new JPanel();
        int players = master.getNumberOfPlayers();
        infoPanel.setLayout(new GridLayout(2, (players+1)/2));
		getContentPane().add(infoPanel, BorderLayout.CENTER);
		data.playerPanels = new PlayerPanel[master.getNumberOfPlayers()];
		for (int i = 0; i< master.getNumberOfPlayers(); i++){
			data.playerPanels[i] = new PlayerPanel(master.getPlayer(i));
			infoPanel.add(data.playerPanels[i]);
			data.playerPanels[i].displayInfo();
		}
	}

	public void enableEndTurnBtn(int playerIndex) {
		data.playerPanels[playerIndex].setEndTurnEnabled(true);
	}
	
	public void enablePlayerTurn(int playerIndex) {
		data.playerPanels[playerIndex].setRollDiceEnabled(true);
		
	}

	public void enablePurchaseBtn(int playerIndex) {
		data.playerPanels[playerIndex].setPurchasePropertyEnabled(true);
	}

	@SuppressWarnings("deprecation")
	public int[] getDiceRoll() {
		TestDiceRollDialog dialog = new TestDiceRollDialog(this);
		dialog.show();
		return dialog.getDiceRoll();
	}

    public boolean isDrawCardButtonEnabled() {
        int currentPlayerIndex = GameMaster.instance().getCurrentPlayerIndex();
        return data.playerPanels[currentPlayerIndex].isDrawCardButtonEnabled();
    }

    public boolean isEndTurnButtonEnabled() {
        int currentPlayerIndex = GameMaster.instance().getCurrentPlayerIndex();
        return data.playerPanels[currentPlayerIndex].isEndTurnButtonEnabled();
    }

	public boolean isGetOutOfJailButtonEnabled() {
		int currentPlayerIndex = GameMaster.instance().getCurrentPlayerIndex();
		return data.playerPanels[currentPlayerIndex].isGetOutOfJailButtonEnabled();
	}

    public boolean isTradeButtonEnabled(int i) {
        return data.playerPanels[i].isTradeButtonEnabled();
    }
	
	public void movePlayer(int index, int from, int to) {
		GUICell fromCell = queryCell(from);
		GUICell toCell = queryCell(to);
		fromCell.removePlayer(index);
		toCell.addPlayer(index);
	}

    @SuppressWarnings("deprecation")
	public RespondDialog openRespondDialog(TradeDeal deal) {
        GUIRespondDialog dialog = new GUIRespondDialog();
        dialog.setDeal(deal);
        dialog.show();
        return dialog;
    }

    @SuppressWarnings("deprecation")
	public TradeDialog openTradeDialog() {
        GUITradeDialog dialog = new GUITradeDialog(this);
        dialog.show();
        return dialog;
    }
	
	private GUICell queryCell(int index) {
		IOwnable cell = GameMaster.instance().getGameBoard().getCell(index);
		for(int x = 0; x < data.guiCells.size(); x++) {
			GUICell guiCell = (GUICell)data.guiCells.get(x);
			if(guiCell.getCell() == cell) return guiCell;
		}
		return null;
	}

    public void setBuyHouseEnabled(boolean b) {
        int currentPlayerIndex = GameMaster.instance().getCurrentPlayerIndex();
        data.playerPanels[currentPlayerIndex].setBuyHouseEnabled(b);
    }

    public void setDrawCardEnabled(boolean b) {
        int currentPlayerIndex = GameMaster.instance().getCurrentPlayerIndex();
        data.playerPanels[currentPlayerIndex].setDrawCardEnabled(b);
    }

    public void setEndTurnEnabled(boolean enabled) {
        int currentPlayerIndex = GameMaster.instance().getCurrentPlayerIndex();
        data.playerPanels[currentPlayerIndex].setEndTurnEnabled(enabled);
    }

    public void setGetOutOfJailEnabled(boolean b) {
        int currentPlayerIndex = GameMaster.instance().getCurrentPlayerIndex();
        data.playerPanels[currentPlayerIndex].setGetOutOfJailEnabled(b);
    }

    public void setPurchasePropertyEnabled(boolean enabled) {
        int currentPlayerIndex = GameMaster.instance().getCurrentPlayerIndex();
        data.playerPanels[currentPlayerIndex].setPurchasePropertyEnabled(enabled);
    }

    public void setRollDiceEnabled(boolean b) {
        int currentPlayerIndex = GameMaster.instance().getCurrentPlayerIndex();
        data.playerPanels[currentPlayerIndex].setRollDiceEnabled(b);
    }

    public void setTradeEnabled(int index, boolean b) {
        data.playerPanels[index].setTradeEnabled(b);
    }
	
	public void setupGameBoard(GameBoard board) {
		Dimension dimension = GameBoardUtil.calculateDimension(board.getCellNumber());
		data.northPanel.setLayout(new GridLayout(1, dimension.width + 2));
		data.southPanel.setLayout(new GridLayout(1, dimension.width + 2));
		data.westPanel.setLayout(new GridLayout(dimension.height, 1));
		data.eastPanel.setLayout(new GridLayout(dimension.height, 1));
		addCells(data.northPanel, GameBoardUtil.getNorthCells(board));
		addCells(data.southPanel, GameBoardUtil.getSouthCells(board));
		addCells(data.eastPanel, GameBoardUtil.getEastCells(board));
		addCells(data.westPanel, GameBoardUtil.getWestCells(board));
		buildPlayerPanels();
	}

    @SuppressWarnings("deprecation")
	public void showBuyHouseDialog(Player currentPlayer) {
        BuyHouseDialog dialog = new BuyHouseDialog(currentPlayer);
        dialog.show();
    }

    public void showMessage(String msg) {
		JOptionPane.showMessageDialog(this, msg);
    }

	public int showUtilDiceRoll() {
		return UtilDiceRoll.showDialog();
	}

	public void startGame() {
		int numberOfPlayers = GameMaster.instance().getNumberOfPlayers();
		for(int i = 0; i < numberOfPlayers; i++) {
			movePlayer(i, 0, 0);
		}
	}

	public void update() {
		for(int i = 0; i < data.playerPanels.length; i++) {
			data.playerPanels[i].displayInfo();
		}
		for(int j = 0; j < data.guiCells.size(); j++ ) {
			GUICell cell = (GUICell)data.guiCells.get(j);
			cell.displayInfo();
		}
	}
}
