package edu.towson.cis.cosc442.project1.monopoly.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.OverlayLayout;
import javax.swing.border.BevelBorder;

import edu.towson.cis.cosc442.project1.monopoly.*;

public class PlayerPanel extends JPanel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PlayerPanelData data = new PlayerPanelData();

	public PlayerPanel(Player player) {
        JPanel pnlAction = new JPanel();
        JPanel pnlInfo = new JPanel();
        data.btnRollDice = new JButton("Roll Dice");
        data.btnPurchaseProperty = new JButton("Purchase Property");
        data.btnEndTurn = new JButton("End Turn");
        data.btnBuyHouse = new JButton("Buy House");
        data.btnGetOutOfJail = new JButton("Get Out of Jail");
        data.btnDrawCard = new JButton("Draw Card");
        data.btnTrade = new JButton("Trade");
        this.data.player = player;
        data.lblName = new JLabel();
        data.lblMoney = new JLabel();
        data.txtProperty = new JTextArea(30, 70);

        data.txtProperty.setEnabled(false);

        JPanel pnlName = new JPanel();
        JPanel pnlProperties = new JPanel();

        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.add(pnlName, BorderLayout.NORTH);
        pnlInfo.add(pnlProperties, BorderLayout.CENTER);

        pnlProperties.setLayout(new OverlayLayout(pnlProperties));

        pnlName.add(data.lblName);
        pnlName.add(data.lblMoney);
        pnlProperties.add(data.txtProperty);

        pnlAction.setLayout(new GridLayout(3, 3));
        pnlAction.add(data.btnBuyHouse);
        pnlAction.add(data.btnRollDice);
        pnlAction.add(data.btnPurchaseProperty);
        pnlAction.add(data.btnGetOutOfJail);
        pnlAction.add(data.btnEndTurn);
        pnlAction.add(data.btnDrawCard);
        pnlAction.add(data.btnTrade);

        pnlAction.doLayout();
        pnlInfo.doLayout();
        pnlName.doLayout();
        pnlProperties.doLayout();
        this.doLayout();

        setLayout(new BorderLayout());
        add(pnlInfo, BorderLayout.CENTER);
        add(pnlAction, BorderLayout.SOUTH);

        data.btnRollDice.setEnabled(false);
        data.btnPurchaseProperty.setEnabled(false);
        data.btnEndTurn.setEnabled(false);
        data.btnBuyHouse.setEnabled(false);
        data.btnGetOutOfJail.setEnabled(false);
        data.btnDrawCard.setEnabled(false);
        data.btnTrade.setEnabled(false);

        setBorder(new BevelBorder(BevelBorder.RAISED));

        data.btnRollDice.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GameMaster.instance().btnRollDiceClicked();
            }
        });

        data.btnEndTurn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GameMaster.instance().btnEndTurnClicked();
            }
        });

        data.btnPurchaseProperty.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GameMaster.instance().btnPurchasePropertyClicked();
            }
        });

        data.btnBuyHouse.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GameMaster.instance().btnBuyHouseClicked();
            }
        });

        data.btnGetOutOfJail.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GameMaster.instance().btnGetOutOfJailClicked();
            }
        });

        data.btnDrawCard.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Card card = GameMaster.instance().btnDrawCardClicked();
                JOptionPane
                        .showMessageDialog(PlayerPanel.this, card.getLabel());
                displayInfo();
            }
        });

        data.btnTrade.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GameMaster.instance().btnTradeClicked();
            }
        });
    }

    public void displayInfo() {
        data.lblName.setText(data.player.getName());
        data.lblMoney.setText("$ " + data.player.getMoney());
        StringBuffer buf = new StringBuffer();
        IOwnable[] cells = data.player.getAllProperties();
        for (int i = 0; i < cells.length; i++) {
            buf.append(cells[i] + "\n");
        }
        data.txtProperty.setText(buf.toString());
    }
    
    public boolean isBuyHouseButtonEnabled() {
        return data.btnBuyHouse.isEnabled();
    }

    public boolean isDrawCardButtonEnabled() {
        return data.btnDrawCard.isEnabled();
    }

    public boolean isEndTurnButtonEnabled() {
        return data.btnEndTurn.isEnabled();
    }
    
    public boolean isGetOutOfJailButtonEnabled() {
        return data.btnGetOutOfJail.isEnabled();
    }
    
    public boolean isPurchasePropertyButtonEnabled() {
        return data.btnPurchaseProperty.isEnabled();
    }
    
    public boolean isRollDiceButtonEnabled() {
        return data.btnRollDice.isEnabled();
    }

    public boolean isTradeButtonEnabled() {
        return data.btnTrade.isEnabled();
    }

    public void setBuyHouseEnabled(boolean b) {
        data.btnBuyHouse.setEnabled(b);
    }

    public void setDrawCardEnabled(boolean b) {
        data.btnDrawCard.setEnabled(b);
    }

    public void setEndTurnEnabled(boolean enabled) {
        data.btnEndTurn.setEnabled(enabled);
    }

    public void setGetOutOfJailEnabled(boolean b) {
        data.btnGetOutOfJail.setEnabled(b);
    }

    public void setPurchasePropertyEnabled(boolean enabled) {
        data.btnPurchaseProperty.setEnabled(enabled);
    }

    public void setRollDiceEnabled(boolean enabled) {
        data.btnRollDice.setEnabled(enabled);
    }

    public void setTradeEnabled(boolean b) {
        data.btnTrade.setEnabled(b);
    }
}