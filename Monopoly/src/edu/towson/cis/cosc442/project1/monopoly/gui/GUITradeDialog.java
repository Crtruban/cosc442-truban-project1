package edu.towson.cis.cosc442.project1.monopoly.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.Iterator;
import java.util.List;

import javax.swing.*;

import edu.towson.cis.cosc442.project1.monopoly.*;

public class GUITradeDialog extends JDialog implements TradeDialog {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GUITradeDialogData data = new GUITradeDialogData();

	public GUITradeDialog(Frame parent) {
        super(parent);
        
        setTitle("Trade Property");
        data.cboSellers = new JComboBox<Object>();
        data.cboProperties = new JComboBox<Object>();
        data.txtAmount = new JTextField();
        data.btnOK = new JButton("OK");
        data.btnCancel = new JButton("Cancel");
        
        data.btnOK.setEnabled(false);
        
        buildSellersCombo();
        setModal(true);
             
        Container contentPane = getContentPane();
        contentPane.setLayout(new GridLayout(4, 2));
        contentPane.add(new JLabel("Sellers"));
        contentPane.add(data.cboSellers);
        contentPane.add(new JLabel("Properties"));
        contentPane.add(data.cboProperties);
        contentPane.add(new JLabel("Amount"));
        contentPane.add(data.txtAmount);
        contentPane.add(data.btnOK);
        contentPane.add(data.btnCancel);
        
        data.btnCancel.addActionListener(new ActionListener(){
            @SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
                GUITradeDialog.this.hide();
            }
        });
        
        data.cboSellers.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e) {
                Player player = (Player)e.getItem();
                updatePropertiesCombo(player);
            }
        });
        
        data.btnOK.addActionListener(new ActionListener() {
            @SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
                int amount = 0;
                try{
                    amount = Integer.parseInt(data.txtAmount.getText());
                } catch(NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(GUITradeDialog.this,
                            "Amount should be an integer", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                Cell cell = (Cell)data.cboProperties.getSelectedItem();
                if(cell == null) return;
                Player player = (Player)data.cboSellers.getSelectedItem();
                Player currentPlayer = GameMaster.instance().getCurrentPlayer();
                if(currentPlayer.getMoney() > amount) { 
	                data.deal = new TradeDeal();
	                data.deal.setAmount(amount);
	                data.deal.setPropertyName(cell.getName());
	                data.deal.setSellerIndex(GameMaster.instance().getPlayerIndex(player));
                }
                hide();
            }
        });
        
        this.pack();
    }

    private void buildSellersCombo() {
        List<?> sellers = GameMaster.instance().getSellerList();
        for (Iterator<?> iter = sellers.iterator(); iter.hasNext();) {
            Player player = (Player) iter.next();
            data.cboSellers.addItem(player);
        }
        if(sellers.size() > 0) {
            updatePropertiesCombo((Player)sellers.get(0));
        }
    }

    public TradeDeal getTradeDeal() {
        return data.deal;
    }

    private void updatePropertiesCombo(Player player) {
        data.cboProperties.removeAllItems();
        IOwnable[] cells = player.getAllProperties();
        data.btnOK.setEnabled(cells.length > 0);
        for (int i = 0; i < cells.length; i++) {
            data.cboProperties.addItem(cells[i]);
        }
    }

}
