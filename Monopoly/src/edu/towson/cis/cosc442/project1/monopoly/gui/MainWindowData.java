package edu.towson.cis.cosc442.project1.monopoly.gui;

import java.util.ArrayList;

import javax.swing.JPanel;

public class MainWindowData {
	public JPanel eastPanel;
	public ArrayList<GUICell> guiCells;
	public JPanel northPanel;
	public PlayerPanel[] playerPanels;
	public JPanel southPanel;
	public JPanel westPanel;

	public MainWindowData(JPanel eastPanel, ArrayList<GUICell> guiCells, JPanel northPanel, JPanel southPanel,
			JPanel westPanel) {
		this.eastPanel = eastPanel;
		this.guiCells = guiCells;
		this.northPanel = northPanel;
		this.southPanel = southPanel;
		this.westPanel = westPanel;
	}
}